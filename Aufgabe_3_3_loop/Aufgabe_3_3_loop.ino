#include <Servo.h>
#include <inttypes.h>

Servo myservo;

int AchseXPin = 56;
int AchseZPin = 57;
int VrefPin = 55;
int ledPin = 13;
int MotorPosPin = 54;
int MotorPwmPin = 3;
float pos = 0;

void blinkLed() {
  for (int i = 0; i < 6; i++) {
    digitalWrite(ledPin, i % 2);
    delay(200);
  }
}

void setup() {
  myservo.attach(MotorPwmPin);
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  bool canblink=(pos>0 && pos<180);
  int VREF = analogRead(VrefPin);
  int RawValX = analogRead(AchseXPin);
  float diff = RawValX - VREF;
  float ValX = ((diff*(3300.0/1024.0)) / 2.0);
  pos += ValX * 0.1;
  Serial.println(pos);
  if (pos < 0) {
    pos = 0;
    if(canblink)
      blinkLed();
      canblink=false;
  }
  else if (pos > 180) {
    pos = 180;
    if(canblink)
    blinkLed();
    canblink=false;
  } else canblink=true;
  myservo.write(pos);
  delay(100);
}


#include "duetimer.h"

bool tasterLastState = HIGH;
bool tasterCurrentState = HIGH;
int tasterCounter = 0;

int tasterPin = 3;
int ledValue = 0;
int ledDirection = 1;

bool tasterflag = false;

int blinkledPin = 13;

void blinkLed() {
  for (int i = 0; i < 6; i++) {
    digitalWrite(blinkledPin, i % 2);
    delay(200);
  }
}

void setup() {
  pinMode(tasterPin, INPUT);
  pinMode(blinkledPin, OUTPUT);
  configureTimer(0, tasterTimer, 1000);
  startTimer(0);
  Serial1.begin(9600);
  Serial.begin(9600);
}

void loop() {
  if (Serial1.read() > -1) {
    Serial.println("blink");
    blinkLed();
  }
  if (tasterflag){
    tasterflag = false;
    tasterAction();
  }
  delay(10);
}

void tasterTimer(void) {
  int tmp;
  //up-->>
  tmp = digitalRead(tasterPin);
  if (tasterCurrentState == tmp) {
    tasterCounter += 1;
  } else {
    tasterCounter = 0;
    tasterCurrentState = tmp;
  }
  if (tasterCounter > 30) {
    if ((tasterLastState == LOW) && (tasterCurrentState == HIGH)) {
      tasterflag = true;
    }
    tasterLastState = tasterCurrentState;
    tasterCounter = 0;
  }
}

void tasterAction(){
  ledValue += (ledDirection*50);
  if (ledValue >= 255){
    ledDirection *= -1;
    ledValue = 255;
  } else if (ledValue <= 0){
    ledDirection *= -1;
    ledValue = 0;
  }
  Serial.println(ledValue);
  Serial1.write(ledValue);
}

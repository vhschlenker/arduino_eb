int ledPin = 7;
int taster1Pin = 5;

bool ledOn = true;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(taster1Pin, INPUT);
  attachInterrupt(taster1Pin, onoff, RISING);
}

void loop() {
  if (ledOn)
  {
    digitalWrite(ledPin, HIGH);
  }
  else
  {
    digitalWrite(ledPin, LOW);
  }

}

void onoff() {
  ledOn = !ledOn;
}

#include <inttypes.h>
#include "duetimer.h"

int ledPin = 7;
int ledVal = 128;

int tasterUpPin = 3;
int tasterDownPin = 5;

int tasterCounter = 0;

int tasterUpLastState = LOW;

int tasterDownLastState = LOW;


void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(tasterUpPin, INPUT);
  pinMode(tasterDownPin, INPUT);

  configureTimer(0, tasterTimer, 1000);
  startTimer(0);
}

void loop() {
  analogWrite(ledPin, ledVal);
}

void tasterTimer(void)
{
  if (tasterCounter > 30)
  {
    if (digitalRead(tasterUpPin) == tasterUpLastState)
    {
      brightup();
    }
    if (digitalRead(tasterDownPin) == tasterDownLastState)
    {
      brightdown();
    }
    tasterCounter = 0;
    tasterDownLastState = LOW;
    tasterDownLastState = LOW;
  } else {
    if (digitalRead(tasterUpPin) == HIGH)
    {
      if (tasterUpLastState == HIGH)
      {
        tasterCounter += 1;
      }
      tasterUpLastState = HIGH;
    }
    if (digitalRead(tasterDownPin) == HIGH)
    {
      if (tasterDownLastState == HIGH)
      {
        tasterCounter += 1;
      }
      tasterDownLastState = HIGH;
    }
  }
}

void brightup() {
  ledVal += 10;
  if (ledVal > 255)
  {
    ledVal = 255;
  }
}

void brightdown() {
  ledVal -= 10;
  if (ledVal < 0)
  {
    ledVal = 0;
  }
}

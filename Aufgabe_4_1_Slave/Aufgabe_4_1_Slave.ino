int ledPin = 3;
int ledValue = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial1.begin(9600);
  Serial.begin(9600);
}

void loop() {
  if (Serial1.available() > 0) {
    // read the incoming byte:
    ledValue = Serial1.read();
    Serial.println(ledValue);
    analogWrite(ledPin,ledValue);
    if (ledValue == 255 || ledValue == 0) {
      Serial1.write(1);
      
    }
  }
}

#include <inttypes.h>
#include "duetimer.h"

int pwmPin = 8;
int MotorAPin = 9;
int MotorBPin = 10;

int MotorSpeed = 0;
int timer = 0;

void setup() {
  pinMode(pwmPin, OUTPUT);
  pinMode(MotorAPin, OUTPUT);
  pinMode(MotorBPin, OUTPUT);
  configureTimer(0, motorTimer, 20);
  startTimer(0);
}

void motorTimer() {
  if (timer < 5000) {
    digitalWrite(MotorAPin, HIGH);
    digitalWrite(MotorBPin, LOW);
    MotorSpeed = timer * 255 / 5000;
  }
  else if (timer < 7000) {
    MotorSpeed = (7000 - timer) * 255 / 2000;
  }
  else if (timer < 12000) {
    digitalWrite(MotorAPin, LOW);
    digitalWrite(MotorBPin, HIGH);
    MotorSpeed = (timer - 7000) * 255 / 5000;
  }
  else if (timer < 14000) {
    MotorSpeed = (14000 - timer) * 255 / 2000;
  }
  timer = (timer+50)%14000;
  analogWrite(pwmPin, MotorSpeed);
}

void loop() {

}

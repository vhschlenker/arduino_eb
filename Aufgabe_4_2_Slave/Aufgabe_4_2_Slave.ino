#include <Wire.h>

int ledPin = 3;
int ledStat = 0;
void setup() {
  pinMode(ledPin, OUTPUT);
  Wire.begin(25);
  Wire.onReceive(ledReceive);
  Wire.onRequest(requestEvent);
  Serial.begin(9600);
}

void loop() {
}

void ledReceive(int numBytes) {
  ledStat = Wire.read();
  analogWrite(ledPin, ledStat * 255);
}

void requestEvent() {
  Wire.write(ledStat);
}

#include <SD.h>
#include <SPI.h>

int pinFIX = 2;
int pinEN = 3;
String satellites;
int LEDPin = 13;

void setup() {
  Serial.begin(9600);
  SD.begin(4);
  ReadGPSLog();
  pinMode(LEDPin, OUTPUT);
  Serial2.begin(9600);
  Serial2.setTimeout(100);
}

void loop() {
  if (Serial2.available() > 0) {
    String rawdata = Serial2.readString();

    int gprmcstart = rawdata.indexOf("$GPRMC");
    int gprmcend = rawdata.indexOf("*", gprmcstart) + 3;
    int gpgsvstart = rawdata.indexOf("$GPGSV");
    int gpgsvend = rawdata.indexOf("*", gpgsvstart) + 3;

    String gprmcdata = rawdata.substring(gprmcstart, gprmcend);
    String gpgsvdata = rawdata.substring(gpgsvstart, gpgsvend);
    //Low = False
    int gpsFixFlag = LOW;
    if (checksum(gprmcdata)) {

      String valid = getString(2, gprmcdata);
      if (valid.equals("A")) {
        float latitude = getString(3, gprmcdata).toFloat() * 0.01f;
        float longitude =  getString(5, gprmcdata).toFloat() * 0.01f;

        if (gpgsvstart > -1) {
          if (checksum(gpgsvdata)) {
            satellites = getString(3, gpgsvdata);
          }
        }
        gpsFixFlag = HIGH;
        Serial.print(latitude);
        Serial.print("N ");
        Serial.print(longitude);
        Serial.print("E ");
        Serial.print(satellites);
        logGPS(latitude,longitude);

      } else {
        Serial.println("not valid");
      }
      Serial.println();
    }
    digitalWrite(LEDPin, gpsFixFlag);
  }
}

int getpos(int i, String s) {
  int pos = 0;
  for (int a = 0; a < i && pos != -1; a++) {
    pos = s.indexOf(",", pos + 1);
  }
  return pos;
}

String getString(int i, String s) {
  return s.substring(getpos(i, s) + 1, getpos(i + 1, s));
}



bool checksum(String s) {
  byte c = 0;

  for (int i = 1; i < s.length() - 3; i++) {
    c ^= s.charAt(i);
  }
  String tmp = String(c, HEX);
  tmp.toUpperCase();
  return ((tmp).equals( s.substring(s.length() - 2, s.length())));
}

void logGPS(float latitude, float longitude) {
  File gpslogFile = SD.open("gps.txt", FILE_WRITE);
  String tmp = "";
  tmp.concat(latitude);
  tmp.concat(",");
  tmp.concat(longitude);
  gpslogFile.println(tmp);
  gpslogFile.close();
}

void ReadGPSLog() {
  File f = SD.open("gps.txt");
  Serial.println("-----------------------------------------------------------------------");
  while(f.available()){
    Serial.print((char) f.read());
  }
  f.close();
  Serial.println("-----------------------------------------------------------------------");
  //SD.remove("gps.txt");
}

//$GPRMC,133640.000,A,5336.0523,N,00956.0106,E,0.68,154.29,280616,,,A*69
//$GPGSV,3,3,11,44,11,118,,10,06,271,,02,04,130,30*45

/*
$GPGSA,A,3,19,06,15,17,24,,,,,,,,2.44,2.28,0.88*02
$GPRMC,084352.000,A,5335.9948,N,00956.0448,E,0.61,287.88,010716,,,A*63
$GPVTG,287.88,T,,M,0.61,N,1.13,K,A*34
$GPGGA,084353.000,5335.9956,N,00956.0434,E,1,5,2.28,29.2,M,45.9,M,,*63
$GPGSA,A,3,19,06,15,17,24,,,,,,,,2.44,2.28,0.88*02
$GPRMC,084353.000,A,5335.9956,N,00956.0434,E,2.25,304.40,010716,,,A*6A
$GPVTG,304.40,T,,M,2.25,N,4.17,K,A*39
$GPGGA,084354.000,5335.9958,N,00956.0424,E,1,5,2.27,29.1,M,45.9,M,,*67
$GPGSA,A,3,19,06,15,17,24,,,,,,,,2.44,2.27,0.88*0D
$GPGSV,3,1,11,12,69,248,,24,68,137,28,19,43,065,36,32,31,308,*7F
$GPGSV,3,2,11,17,24,044,19,14,20,318,,06,16,093,31,15,13,182,35*7F
$GPGSV,3,3,11,44,11,118,,10,06,271,,02,04,130,30*45
$GPRMC,084354.000,A,5335.9958,N,00956.0424,E,3.73,303.66,010716,,,A*63
$GPVTG,303.66,T,,M,3.73,N,6.91,K,A*34
$GPGGA,084355.000,5335.9963,N,00956.0412,E,1,5,2.28,29.1,M,45.9,M,,*64
$GPGSA,A,3,19,06,15,17,24,,,,,,,,2.44,2.28,0.88*02
$GPRMC,084355.000,A,5335.9963,N,00956.0412,E,4.26,304.10,010716,,,A*6E
$GPVTG,304.10,T,,M,4.26,N,7.89,K,A*3D
$GPGGA,084356.000,5335.9968,N,00956.0398,E,1,5,2.43,29.0,M,45.9,M,,*65
$GPGSA,A,3,19,06,15,17,24,,,,,,,,3.07,2.43,1.87*07
$GPRMC,084356.000,A,5335.9968,N,00956.0398,E,3.85,304.36,010716,,,A*69
$GPVTG,304.36,T,,M,3.85,N,7.13,K,A*34
*/

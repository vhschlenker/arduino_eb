#include <inttypes.h>
#include "duetimer.h"

int AchseXPin = 55;
int AchseZPin = 56;
int VrefPin = 54;

void setup() {
  configureTimer(0, tasterGyro, 10);
  startTimer(0);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

}

void tasterGyro() {
  int VREF = analogRead(VrefPin);
  int RawValX = analogRead(AchseXPin);
  int ValX = ((RawValX-VREF)/2);
  int RawValZ = analogRead(AchseZPin);
  int ValZ = ((RawValZ-VREF)/2);
  Serial.print(" X= ");
  Serial.print(ValX);
  Serial.print(" Z= ");
  Serial.println(ValZ);
} 

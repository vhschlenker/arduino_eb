#include <Wire.h>

int AchseXPin = 56;
int VrefPin = 55;

void setup() {
  pinMode(AchseXPin, INPUT);
  pinMode(VrefPin, INPUT);
  Wire.begin(25);
  Wire.onRequest(requestEvent);
  Serial.begin(9600);
}

void loop() {
}

void requestEvent() {
  int VREF = analogRead(VrefPin);
  int RawValX = analogRead(AchseXPin);
  float diff = RawValX - VREF;
  float ValX = ((diff*(3300.0/1024.0)) / 2.0);
  int i = (int) (ValX*1000.0);
  Serial.println(ValX);
  Wire.write((i    ) & 255);
  Wire.write((i>> 8) & 255);
  Wire.write((i>>16) & 255);
  Wire.write((i>>24) & 255);
  }



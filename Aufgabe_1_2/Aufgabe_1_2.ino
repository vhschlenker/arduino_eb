int ledPin = 7;
int taster1Pin = 5;

bool ledOn = true;
int lastlooped = LOW;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(taster1Pin, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int tmp = digitalRead(taster1Pin);
  if ((tmp == HIGH) && (lastlooped == LOW))
  {
   ledOn = !ledOn;
  }
  lastlooped = tmp;
  if (ledOn)
    {
      digitalWrite(ledPin,HIGH);
    }
  else
    {
      digitalWrite(ledPin,LOW);
    }
}

#include <Servo.h>

Servo myservo;
String inputString = "";
int MotorPwmPin = 3;

void setup() {
  myservo.attach(MotorPwmPin);
  Serial.begin(9600);
}

void loop() {
  if (Serial.available() > 0) {
    // read the incoming byte:
    inputString = Serial.readString();
    //Serial.print(inputString);
    if (inputString.startsWith("moveServoTo(") && inputString.endsWith(")")) {
      String tmp = inputString.substring(12, inputString.length() - 1);

      //Serial.print(" --- ");
      Serial.println(tmp);
      int tmp2 = tmp.toInt();
      if (tmp2 <= 180 && tmp2 >= 0) {
        myservo.write(tmp2);
      }

    }

  }
}

#include <Servo.h>

Servo myservo;

int MotorPosPin = 54;
int MotorPwmPin = 3;
float pos = 0;

void setup() {
  myservo.attach(MotorPwmPin);
}

void loop() {
  myservo.write(90+(cos(pos)*90));
  pos += 0.01;
  delay(10);
}

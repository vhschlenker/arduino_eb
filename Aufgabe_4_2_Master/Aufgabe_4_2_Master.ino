#include <Wire.h>

void setup() {
  Wire.begin();
  Serial.begin(9600);
}

void loop() {
  Wire.beginTransmission(25);
  Wire.write(1);
  Wire.endTransmission();
  Wire.requestFrom(25, 1);
  delay(10);
  if (Wire.available()) {
    Serial.print("Slave is on: ");
    Serial.println(Wire.read());
  }
  delay(2000);
  Wire.beginTransmission(25);
  Wire.write(0);
  Wire.endTransmission();
  Wire.requestFrom(25, 1);
  delay(10);
  if (Wire.available()) {
    Serial.print("Smave is on: ");
    Serial.println(Wire.read());
  }
  delay(2000);
}

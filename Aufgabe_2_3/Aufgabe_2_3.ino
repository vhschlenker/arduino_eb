#include <inttypes.h>
#include "duetimer.h"

bool tasterUpLastState = HIGH;
bool tasterUpCurrentState = HIGH;
int tasterUpCounter = 0;

bool tasterDownLastState = HIGH;
bool tasterDownCurrentState = HIGH;
int tasterDownCounter = 0;

int pwmPin = 8;
int MotorAPin = 9;
int MotorBPin = 10;
int tasterUpPin = 3;
int tasterDownPin = 5;

int MotorSpeed = 100;
int MotorSpeedChange = 20;
int taster1Mode = 0;

void setup() {
  pinMode(pwmPin, OUTPUT);
  pinMode(MotorAPin, OUTPUT);
  pinMode(MotorBPin, OUTPUT);
  pinMode(tasterUpPin, INPUT);
  pinMode(tasterDownPin, INPUT);
  configureTimer(0, tasterTimer, 1000);
  startTimer(0);
  configureTimer(1, motorTimer, 20);
  startTimer(1);
  digitalWrite(MotorAPin, HIGH);
    digitalWrite(MotorBPin, LOW);
    Serial.begin(9600);
}

void motorTimer() {
  if ((taster1Mode == 2 )&& (MotorSpeed > 0)) {
    MotorSpeed = MotorSpeed-5;
  }
}

void tasterTimer(void) {
  int tmp;
  //up-->>
  tmp = digitalRead(tasterUpPin);
  if (tasterUpCurrentState == tmp) {
    tasterUpCounter += 1;
  } else {
    tasterUpCounter = 0;
    tasterUpCurrentState = tmp;
  }
  if (tasterUpCounter > 30) {
    if ((tasterUpLastState == LOW) && (tasterUpCurrentState == HIGH)) {
      taster2Action();
    }
    tasterUpLastState = tasterUpCurrentState;
    tasterUpCounter = 0;
  }
  //down-->>
  tmp = digitalRead(tasterDownPin);
  if (tasterDownCurrentState == tmp) {
    tasterDownCounter += 1;
  } else {
    tasterDownCounter = 0;
    tasterDownCurrentState = tmp;
  }
  if (tasterDownCounter > 30) {
    if ((tasterDownLastState == LOW) && (tasterDownCurrentState == HIGH)) {
      taster1Action();
    }
    tasterDownLastState = tasterDownCurrentState;
    tasterDownCounter = 0;
  }
}

void taster2Action() {
  MotorSpeed += MotorSpeedChange;
  if (MotorSpeed > 255) {
    MotorSpeed = 255;
    MotorSpeedChange=MotorSpeedChange*(-1);
  }
  if (MotorSpeed < 0) {
    MotorSpeed = 0;
    MotorSpeedChange=MotorSpeedChange*(-1);
  }
}

void taster1Action() {
  taster1Mode = (taster1Mode + 1) % 3;
  if (taster1Mode == 0) {
    digitalWrite(MotorAPin, HIGH);
    digitalWrite(MotorBPin, LOW);
  }
  else  if (taster1Mode == 1) {
    digitalWrite(MotorAPin, LOW);
    digitalWrite(MotorBPin, HIGH);
  }
}

void loop() {
  analogWrite(pwmPin, MotorSpeed);
  Serial.println(MotorSpeed);
}

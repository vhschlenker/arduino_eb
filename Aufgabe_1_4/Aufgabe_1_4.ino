int ledPin = 7;
int ledVal = 0;

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  ledVal = (ledVal+1)%256 ;
  analogWrite(ledPin, ledVal);
  Serial.println(ledVal);
  delay(100);
}

int ledPin = 7; 

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);  
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledPin, HIGH);   // sets the LED on
  delay(2000);                  // waits for a second
  digitalWrite(ledPin, LOW);    // sets the LED off
  delay(500);      
}

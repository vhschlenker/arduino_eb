#include "duetimer.h"
#include <Servo.h>
#include <inttypes.h>

Servo myservo;

int AchseXPin = 56;
int AchseZPin = 57;
int VrefPin = 55;
int ledPin = 2;
int MotorPosPin = 54;
int MotorPwmPin = 3;
float pos = 0;

void blinkLed() {
  for (int i = 0; i < 6; i++) {
    digitalWrite(ledPin, i % 2);
    delay(200);
  }
}

void setup() {
  configureTimer(0, tasterGyro, 10);
  startTimer(0);
  myservo.attach(MotorPwmPin);
  pinMode(ledPin, OUTPUT);
  //Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

}

void tasterGyro() {
  int VREF = analogRead(VrefPin);
  int RawValX = analogRead(AchseXPin);
  int ValX = ((RawValX - VREF) / 2);
  int RawValZ = analogRead(AchseZPin);
  int ValZ = ((RawValZ - VREF) / 2);
  pos += ValX * 0.1;
  if (pos < 0) {
    pos = 0;
    blinkLed();
  }
  if (pos > 180) {
    pos = 180;
    blinkLed();
  }
  myservo.write(pos);
}


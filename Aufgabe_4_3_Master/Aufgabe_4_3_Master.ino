#include <Servo.h>
#include <Wire.h>

Servo myservo;

int MotorPwmPin = 3;
float pos = 0;

void setup() {
  myservo.attach(MotorPwmPin);
  Wire.begin();
  Serial.begin(9600);
}

void loop() {
  Wire.requestFrom(25, 4);
  if (Wire.available()) {
    Serial.print("Slave is on: ");
    int i=(Wire.read() & 255);
    i+=   (Wire.read() & 255)<<8;
    i+=   (Wire.read() & 255)<<16;
    i+=   (Wire.read() & 255)<<24;
    float ValX = (float) (i*0.001);
    Serial.println(ValX);
    pos += ValX * 0.05;
    if (pos < 0) {
      pos = 0;
    }
    else if (pos > 180) {
      pos = 180;
    }
    myservo.write(pos);
  }
  delay(50);
}

#include <inttypes.h>
#include "duetimer.h"

int ledPin = 13;
int ledVal = 128;

int tasterUpPin = 14;
int tasterDownPin = 16;

int tasterCounter = 0;

bool tasterUpLastState = false;
bool tasterUpCurrentState = false;
bool tasterUpValid = true;

bool tasterDownLastState = false;
bool tasterDownCurrentState = false;
bool tasterDownValid = true;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(tasterUpPin, INPUT);
  pinMode(tasterDownPin, INPUT);

  configureTimer(0, tasterTimer, 1000);
  startTimer(0);
}

void loop() {
  analogWrite(ledPin, ledVal);
}

void tasterTimer(void)
{
  tasterCounter += 1;
  if (tasterCounter > 30)
  {
    if(tasterUpValid ){
      if((tasterUpCurrentState) && (!tasterUpLastState )) {
        brightup();
      }
      tasterUpLastState=tasterUpCurrentState;
    }
    if(tasterDownValid ){
      if((tasterDownCurrentState) && (!tasterDownLastState)) {
        brightdown();
      }
      tasterDownLastState=tasterDownCurrentState;
    }
    
    tasterCounter=0;
    tasterUpValid=true;
    tasterUpCurrentState=(digitalRead(tasterUpPin) == HIGH);
    tasterDownValid=true;
    tasterDownCurrentState=(digitalRead(tasterDownPin) == HIGH);
    
  } else {
    if ((digitalRead(tasterUpPin) == HIGH) != tasterUpCurrentState)
    {
      tasterUpValid=false;
    }
    if ((digitalRead(tasterDownPin) == HIGH) != tasterDownCurrentState)
    {
      tasterDownValid=false;
    }
  }
}

void brightup() {
  ledVal += 10;
  if (ledVal > 255)
  {
    ledVal = 255;
  }
}

void brightdown() {
  ledVal -= 10;
  if (ledVal < 0)
  {
    ledVal = 0;
  }
}

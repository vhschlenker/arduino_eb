int ledPin = 7;
int ledVal = 0;

int tasterUpPin = 3;
int tasterDownPin = 5;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(tasterUpPin, INPUT);
  pinMode(tasterDownPin, INPUT);
  Serial.begin(9600);
}

void loop() {
  if (digitalRead(tasterUpPin) == HIGH)
  {
    ledVal += 10;
    if (ledVal > 255)
    {
      ledVal = 255;
    }
  }
  if (digitalRead(tasterDownPin) == HIGH)
  {
    ledVal -= 10;
    if (ledVal < 0)
    {
      ledVal = 0;
    }
  }

  analogWrite(ledPin, ledVal);
  Serial.println(ledVal);
  delay(100);
}

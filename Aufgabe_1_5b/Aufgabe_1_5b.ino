int ledPin = 13;
int ledVal = 0;

int tasterUpPin = 3;
int tasterDownPin = 5;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(tasterUpPin, INPUT);
  pinMode(tasterDownPin, INPUT);
  attachInterrupt(tasterUpPin, brightup, RISING);
  attachInterrupt(tasterDownPin, brightdown, RISING);
  //Serial.begin(9600);
}

void loop() {
  analogWrite(ledPin, ledVal);
  //Serial.println(ledVal);
}

void brightup() {
  ledVal += 10;
  if (ledVal > 255)
  {
    ledVal = 255;
  }
  //Serial.println("up");
}

void brightdown() {
  ledVal -= 10;
  if (ledVal < 0)
  {
    ledVal = 0;
  }
  //Serial.println("down");
}

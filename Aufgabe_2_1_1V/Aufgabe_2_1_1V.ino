#include <inttypes.h>
#include "duetimer.h"

bool tasterUpLastState = HIGH;
bool tasterUpCurrentState = HIGH;
int tasterUpCounter = 0;

bool tasterDownLastState = HIGH;
bool tasterDownCurrentState = HIGH;
int tasterDownCounter = 0;

int ledVal = 128;

int tasterUpPin = 3;
int tasterDownPin = 5;
int ledPin = 7;

void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(tasterUpPin, INPUT);
  pinMode(tasterDownPin, INPUT);
  configureTimer(0,tasterTimer, 1000);
  startTimer(0);
}

void tasterTimer(void) {
  int tmp;
  //up-->>
  tmp = digitalRead(tasterUpPin);
  if (tasterUpCurrentState == tmp) {
    tasterUpCounter += 1;
  } else{
    tasterUpCounter = 0;
    tasterUpCurrentState = tmp;
  }
  if(tasterUpCounter > 30) {
    if((tasterUpLastState == LOW) && (tasterUpCurrentState == HIGH)) {
      brightup();
    }
    tasterUpLastState = tasterUpCurrentState;
    tasterUpCounter = 0;
  }
  //down-->>
  tmp = digitalRead(tasterDownPin);
  if (tasterDownCurrentState == tmp) {
    tasterDownCounter += 1;
  } else{
    tasterDownCounter = 0;
    tasterDownCurrentState = tmp;
  }
  if(tasterDownCounter > 30) {
    if((tasterDownLastState == LOW) && (tasterDownCurrentState == HIGH)) {
      brightdown();
    }
    tasterDownLastState = tasterDownCurrentState;
    tasterDownCounter = 0;
  }
}

void brightup() {
  ledVal += 100;
  if (ledVal > 255) {
    ledVal = 255;
  }
}

void brightdown() {
  ledVal -= 100;
  if (ledVal < 0) {
    ledVal = 0;
  }  
}

void loop() {
  analogWrite(ledPin, ledVal);
}

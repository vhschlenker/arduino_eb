int ledPin = 13;
String inputString = "";
#define LED_on "LED_on"
#define LED_off "LED_off"

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600); 
}

void loop() {
        if (Serial.available() > 0) {
                // read the incoming byte:
                inputString = Serial.readString();

                // say what you got:
                //Serial.print("I received: ");
                //Serial.println(incomingByte, DEC);
                if (inputString.equals(LED_on)){
                  digitalWrite(ledPin,HIGH);
                }
                if (inputString.equals(LED_off)){
                  digitalWrite(ledPin,LOW);
                }
        }
}
